
from django.urls import path

from accounts.views import *

app_name = 'accounts'

urlpatterns = [
    path('', redirect_view),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('signup/person/', PersonSignUpView.as_view(), name='person-signup'),
    path('signup/library/', LibrarySignUpView.as_view(), name='library-signup'),
    path('profile/<int:pk>/', ProfileView.as_view(), name='profile'),
    path('profile/<int:pk>/update/', ProfileUpdateView.as_view(), name='profile-update'),
    path('profile/library/<int:pk>/update/', LibraryProfileUpdateView.as_view(), name='library-update'),
    path('profile/add-library-cards', LibraryCardsCreateView.as_view(), name='library-cards-create'),
    path('profile/library-cards/list', LibraryCardsList.as_view(), name='library-cards-list'),
    path('profile/library-cards/<int:pk>/update', LibraryCardsUpdate.as_view(), name='library-cards-update'),
    path('profile/library-cards/<int:pk>/delete', LibraryCardsDelete.as_view(), name='library-cards-delete'),
]