from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse

from accounts.forms import ProfileUpdateForm, LibraryProfileUpdateForm
from accounts.models import User, Library, Person, LibraryCards
from digital_lending.models import Region, Province, City


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(email="test1@email.com", username="test1",
                                 first_name="alice", last_name="rossi",
                                 password="passwordtest1", is_private=False,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="test2@email.com", username="test2",
                                 first_name="bob", last_name="verdi",
                                 password="passwordtest2", is_private=True,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="test3@email.com", username="bibliotest3",
                                 password="passwordtest3", is_private=False,
                                 is_person=False, is_library=True)
        User.objects.create_user(email="test4@email.com", username="bibliotest4",
                                 password="passwordtest4", is_private=False,
                                 is_person=False, is_library=True)
        User.objects.create_user(email="test5@email.com", username="test5",
                                 last_name="prova5",
                                 password="passwordtest5", is_private=True,
                                 is_person=True, is_library=False)
        Region.objects.create(name="Emilia Romagna")
        Province.objects.create(name="Modena", region=Region.objects.get(name="Emilia Romagna"))
        City.objects.create(name="Carpi", province=Province.objects.get(name="Modena"))
        Library.objects.create(user=User.objects.get(username="bibliotest3"),
                               name="biblioteca3",
                               city=City.objects.get(name="Carpi"))

        Person.objects.create(user=User.objects.get(username="test1"))

    def test_user_setup(self):
        self.assertEqual(User.objects.all().count(), 5)

    def test_user_create_choice(self):
        response = self.client.get(reverse('accounts:signup'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Registrati come persona")
        self.assertContains(response, "Registrati come biblioteca")

    def test_user_login_success(self):
        response = self.client.login(
            username='test1',
            password='passwordtest1'
        )
        self.assertTrue(response)

    def test_user_login_fail(self):
        response = self.client.login(
            username='test1',
            password='wrongpassword2'
        )
        self.assertFalse(response)

    def test_user_login_redirect_to_home(self):
        response = self.client.post(reverse('login'), {
            'username': 'test1',
            'password': 'passwordtest1'
        }, follow=True)
        self.assertRedirects(response, reverse('home'))

    def test_public_profile_view(self):
        response = self.client.get(
            reverse('accounts:profile', kwargs={'pk': 1})
        )
        account = response.context['profile']
        self.assertEqual(account.username, 'test1')
        self.assertEqual(account.email, 'test1@email.com')
        self.assertNotContains(response, "Profilo privato")

        self.assertNotContains(response, "Modifica dati profilo")
        self.assertNotContains(response, "Modifica biblioteche")
        self.assertNotContains(response, "Cronologia prestiti")
        self.assertNotContains(response, "Prenotazioni in corso")
        self.assertNotContains(response, "Preferiti")
        self.assertNotContains(response, "Le mie liste")
        self.assertNotContains(response, "Liste salvate")
        self.assertNotContains(response, "Le mie recensioni")

    def test_private_profile_view(self):
        response = self.client.get(
            reverse('accounts:profile', kwargs={'pk': 2})
        )
        self.assertContains(response, "Profilo privato")

        self.assertNotContains(response, "Modifica dati profilo")
        self.assertNotContains(response, "Modifica biblioteche")
        self.assertNotContains(response, "Cronologia prestiti")
        self.assertNotContains(response, "Prenotazioni in corso")
        self.assertNotContains(response, "Preferiti")
        self.assertNotContains(response, "Le mie liste")
        self.assertNotContains(response, "Liste salvate")
        self.assertNotContains(response, "Le mie recensioni")

    def test_own_profile_view(self):
        self.client.login(
            username='test5',
            password='passwordtest5'
        )
        response = self.client.get(
            reverse('accounts:profile', kwargs={'pk': 5})
        )
        account = response.context['profile']
        self.assertEqual(account.username, 'test5')
        self.assertEqual(account.email, 'test5@email.com')
        self.assertEqual(account.last_name, 'prova5')
        self.assertNotContains(response, "Profilo privato")

        self.assertContains(response, "Modifica dati profilo")
        self.assertContains(response, "Modifica biblioteche")
        self.assertContains(response, "Cronologia prestiti")
        self.assertContains(response, "Prenotazioni in corso")
        self.assertContains(response, "Preferiti")
        self.assertContains(response, "Le mie liste")
        self.assertContains(response, "Liste salvate")
        self.assertContains(response, "Le mie recensioni")

    def test_anonymous_user_browsing(self):
        response = self.client.get(
            reverse('digital_lending:editions')
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            reverse('digital_lending:periodicals')
        )
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            reverse('digital_lending:book-lists')
        )
        self.assertEqual(response.status_code, 200)

    def test_user_profile_update_valid_input(self):
        self.client.login(
            username='test2',
            password='passwordtest2'
        )

        profile_update_form_data = {
            'username': 'pers1',
            'last_name': 'test1'
        }

        profile_update_form = ProfileUpdateForm(data=profile_update_form_data)
        self.assertTrue(profile_update_form.is_valid())

    def test_user_profile_update_not_valid_input(self):
        self.client.login(
            username='bibliotest3',
            password='passwordtest3'
        )

        profile_update_form_data = {
            'username': '',
            'email': 'not_valid_email'
        }

        profile_update_form = ProfileUpdateForm(data=profile_update_form_data)
        self.assertFalse(profile_update_form.is_valid())


class UserPersonTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(email="persona1@email.com", username="persona1",
                                 first_name="alice", last_name="rossi",
                                 password="passwordtest1", is_private=False,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="persona2@email.com", username="persona2",
                                 first_name="bob", last_name="verdi",
                                 password="passwordtest2", is_private=True,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="biblioteca33@email.com", username="persona3",
                                 password="passwordtest3", is_private=False,
                                 is_person=False, is_library=True)
        Region.objects.create(name="Emilia Romagna")
        Province.objects.create(name="Modena", region=Region.objects.get(name="Emilia Romagna"))
        City.objects.create(name="Carpi", province=Province.objects.get(name="Modena"))
        Person.objects.create(user=User.objects.get(username="persona1"))
        Library.objects.create(user=User.objects.get(username="persona3"),
                               name="biblioteca1",
                               city=City.objects.get(name="Carpi"))

    def test_user_create(self):
        response = self.client.post(reverse('accounts:person-signup'), {
            'email': 'persona4@email.com',
            'username': 'persona4',
            'first_name': 'pers4',
            'last_name': 'pers',
            'is_private': False,
            'libraries': Library.objects.get(name="biblioteca1"),
            'library_card': "123456789102"
        })

        self.assertEqual(response.status_code, 200)

    def test_user_create_valid_library_card(self):
        self.client.login(
            username='persona1',
            password='passwordtest1'
        )

        response = self.client.post(reverse('accounts:library-cards-create'), {
            'person': 1,
            'library': 3,
            'number_code': '121212121212'
        })

        self.assertRedirects(response, reverse('accounts:library-cards-list'))
        self.assertEqual(LibraryCards.objects.count(), 1)
        self.assertTrue(LibraryCards.objects.get(person_id=1, library_id=3, number_code='121212121212'))

    def test_user_create_not_valid_library_card(self):
        self.client.login(
            username='persona1',
            password='passwordtest1'
        )

        response = self.client.post(reverse('accounts:library-cards-create'), {
            'person': 1,
            'library': 3,
            'number_code': '12345'
        })
        self.assertEqual(LibraryCards.objects.count(), 0)
        self.assertContains(response, "Il codice deve essere lungo 12 cifre")

    def test_user_create_library_cards_for_same_library(self):
        self.client.login(
            username='persona1',
            password='passwordtest1'
        )

        LibraryCards.objects.create(person_id=1, library_id=3, number_code='121212121212')

        with self.assertRaises(IntegrityError) as context:
            LibraryCards.objects.create(person_id=1, library_id=3, number_code='212121212121')

        self.assertTrue('UNIQUE constraint failed' in str(context.exception))


class UserLibraryTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(email="persona1@email.com", username="persona1",
                                 first_name="alice", last_name="rossi",
                                 password="passwordtest1", is_private=False,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="persona2@email.com", username="persona2",
                                 first_name="bob", last_name="verdi",
                                 password="passwordtest2", is_private=True,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="biblioteca1@email.com", username="biblio1",
                                 password="passwordtest3", is_private=False,
                                 is_person=False, is_library=True)
        Region.objects.create(name="Emilia Romagna")
        Province.objects.create(name="Modena", region=Region.objects.get(name="Emilia Romagna"))
        City.objects.create(name="Carpi", province=Province.objects.get(name="Modena"))
        Library.objects.create(user=User.objects.get(username="biblio1"),
                               name="biblioteca1",
                               city=City.objects.get(name="Carpi"))

    def test_user_create(self):
        response = self.client.post(reverse('accounts:library-signup'), {
            'email': 'biblioteca2@email.com',
            'username': 'biblio2',
            'name': 'biblioteca2',
            'is_private': False,
            'city': City.objects.get(name="Carpi"),
            'address': "via prova 2"
        })

        self.assertEqual(response.status_code, 200)

    def test_library_profile_update_valid_input(self):
        self.client.login(
            username='biblio1',
            password='passwordtest3'
        )

        library_profile_update_form_data = {
            'name': 'biblioteca1',
            'city': 1,
            'address': 'via test 1',
            'phone_number': '059875644'
        }

        library_profile_update_form = LibraryProfileUpdateForm(data=library_profile_update_form_data)
        self.assertTrue(library_profile_update_form.is_valid())

    def test_library_profile_update_not_valid_input(self):
        self.client.login(
            username='biblio1',
            password='passwordtest3'
        )

        library_profile_update_form_data = {
            'address': '',
        }

        library_profile_update_form = LibraryProfileUpdateForm(data=library_profile_update_form_data)
        self.assertFalse(library_profile_update_form.is_valid())
