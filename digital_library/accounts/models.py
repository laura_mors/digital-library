from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from digital_lending.models import City


class User(AbstractUser):
    is_person = models.BooleanField(default=False, verbose_name='utente è persona')
    is_library = models.BooleanField(default=False)
    is_private = models.BooleanField(default=False)
    profile_pic = models.ImageField(upload_to='images/', null=True, blank=True)

    def __str__(self):
        return self.username


class Library(models.Model):
    user = models.OneToOneField(User, related_name='libraries', on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=50)
    city = models.ForeignKey(City, related_name='libraries', on_delete=models.CASCADE, max_length=50)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=13)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Libraries"


class Person(models.Model):
    user = models.OneToOneField(User, related_name='people', on_delete=models.CASCADE, primary_key=True)
    libraries = models.ManyToManyField(Library, through='LibraryCards')

    def __str__(self):
        return self.user.username


class LibraryCards(models.Model):
    person = models.ForeignKey(Person, related_name='library_cards', on_delete=models.CASCADE)
    library = models.ForeignKey(Library, related_name='library_cards', on_delete=models.CASCADE)
    date_added = models.DateField(auto_now_add=True)
    number_code = models.CharField(validators=[RegexValidator(regex='^.{12}$',
                                                              message='Il codice deve essere lungo 12 cifre')],
                                   max_length=12)

    def __str__(self):
        return self.number_code

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['person', 'library'],
                                    name='Each user can have only one library card per library')
        ]
        verbose_name_plural = "Library cards"



