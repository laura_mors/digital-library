from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import CreateView, TemplateView, UpdateView, ListView, DeleteView

from accounts.decorators import person_required
from .forms import ProfileUpdateForm, LibraryCardsForm, LibraryProfileUpdateForm
from .models import Person, Library, LibraryCards

from .models import User
from .forms import PersonSignUpForm, LibrarySignUpForm


class SignUpView(TemplateView):
    template_name = 'registration/signup.html'


class PersonSignUpView(SuccessMessageMixin, CreateView):
    model = User
    form_class = PersonSignUpForm
    template_name = 'registration/signup_form.html'
    success_message = 'Utente creato con successo!'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'person'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('home')


class LibrarySignUpView(SuccessMessageMixin, CreateView):
    model = User
    form_class = LibrarySignUpForm
    template_name = 'registration/signup_form.html'
    success_message = 'Biblioteca creata con successo!'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'library'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('home')


def redirect_view(request):
    response = redirect('/accounts/signup')
    return response


class ProfileView(View):
    profile = None

    def get(self, request, **kwargs):
        account = get_object_or_404(User, id=self.kwargs['pk'])
        context = {'profile': account}
        template_name = ''
        if account.is_person:
            template_name = 'accounts/person/profile.html'
            all_libraries = LibraryCards.objects.all().filter(
                person_id=account.id).exclude(
                library__user__username="Open")
            libraries = [Library.objects.get(user_id=lib.library_id) for lib in all_libraries]
            context.update({'libraries': libraries})

        elif account.is_library:
            template_name = 'accounts/library/profile.html'
            library_user = Library.objects.get(user_id=account.id)
            context.update({'library': library_user})

        if account.is_private and account.id != self.request.user.id:
            context['view_profile'] = False
        else:
            context['view_profile'] = True

        return render(request, template_name, context)


class ProfileUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    template_name = 'accounts/update.html'
    form_class = ProfileUpdateForm
    success_message = 'Profilo aggiornato con successo!'
    permission_denied_message = 'Devi prima effettuare il login!'

    def get_success_url(self):
        pk = self.request.user.id
        return reverse_lazy('accounts:profile', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(ProfileUpdateView, self).get_object(*args, **kwargs)
        if not obj.id == self.request.user.id:
            raise PermissionDenied
        return obj


class LibraryProfileUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Library
    template_name = 'accounts/library/update.html'
    form_class = LibraryProfileUpdateForm
    success_message = 'Dati della biblioteca aggiornati con successo!'
    permission_denied_message = 'Devi prima effettuare il login!'

    def get_success_url(self):
        pk = self.request.user.id
        return reverse_lazy('accounts:profile', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(LibraryProfileUpdateView, self).get_object(*args, **kwargs)
        if not obj.user_id == self.request.user.id:
            raise PermissionDenied
        return obj


@method_decorator(person_required, name='dispatch')
class LibraryCardsCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = LibraryCards
    template_name = 'accounts/library_cards/create.html'
    form_class = LibraryCardsForm
    success_url = reverse_lazy('accounts:library-cards-list')
    success_message = 'Biblioteca aggiunta con successo!'
    permission_denied_message = "Devi prima effettuare il login!"

    def get_object(self, *args, **kwargs):
        obj = super(LibraryCardsCreateView, self).get_object(*args, **kwargs)
        if not obj.user_id == self.request.user.id:
            raise PermissionDenied
        return obj

    def form_valid(self, form):
        form.instance.person = Person.objects.get(user_id=self.request.user.id)
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        try:
            return super().post(request, *args, **kwargs)
        except IntegrityError:
            messages.add_message(request, messages.ERROR,
                                 'Hai già creato una tessera bibliotecaria per questa biblioteca.')
            return render(request, template_name=self.template_name, context=self.get_context_data())


@method_decorator(person_required, name='dispatch')
class LibraryCardsUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = LibraryCards
    template_name = 'accounts/library_cards/update.html'
    form_class = LibraryCardsForm
    success_url = reverse_lazy('accounts:library-cards-list')
    success_message = 'Biblioteca aggiornata con successo!'


@method_decorator(person_required, name='dispatch')
class LibraryCardsDelete(LoginRequiredMixin, DeleteView):
    model = LibraryCards
    template_name = 'accounts/library_cards/delete.html'
    success_url = reverse_lazy('accounts:library-cards-list')
    success_message = 'Biblioteca eliminata con successo!'

    def delete(self, request, *args, **kwargs):
        obj = super(LibraryCardsDelete, self).delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return obj


@method_decorator(person_required, name='dispatch')
class LibraryCardsList(LoginRequiredMixin, ListView):
    paginate_by = 6
    model = LibraryCards
    template_name = 'accounts/library_cards/list.html'

    def get_queryset(self):
        library_cards = LibraryCards.objects.filter(
            person_id=self.request.user.id).exclude(
                library__user__username="Open")
        return library_cards.order_by('-date_added')



