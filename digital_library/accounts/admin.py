from django.contrib import admin
from .models import User, Person, Library, LibraryCards


class UserTypesFilter(admin.SimpleListFilter):
    title = 'tipo utente'
    parameter_name = 'user_type'

    def lookups(self, request, model_admin):
        return (
            ('person', 'Person'),
            ('library', 'Library'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'person':
            return queryset.filter(is_person=True)
        elif self.value() == 'library':
            return queryset.filter(is_library=True)
        return queryset


@admin.register(User)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('email', 'username', 'date_joined', 'last_login', 'is_staff', 'is_person', 'is_library')
    search_fields = ('email', 'username')
    readonly_fields = ('last_login', 'date_joined')
    list_filter = (UserTypesFilter,)


admin.site.register(Person)
admin.site.register(Library)
admin.site.register(LibraryCards)
