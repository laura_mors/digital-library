import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.validators import RegexValidator
from django.db import transaction

from digital_lending.models import City
from .models import User, Person, Library, LibraryCards


class PersonSignUpForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(PersonSignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['libraries'].label = 'Biblioteca'

    library_card = forms.CharField(validators=[RegexValidator(regex='^.{12}$',
                                                              message='Il codice deve essere lungo 12 cifre.')],
                                   max_length=12, label="Tessera bibliotecaria")
    libraries = forms.ModelChoiceField(queryset=Library.objects.all().exclude(
        user__username="Open").order_by('name'))

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'password1', 'password2',
                  'first_name', 'last_name', 'email',
                  'libraries', 'library_card', 'is_private',
                  'profile_pic')
        labels = {
            'library_card': 'Tessera bibliotecaria',
            'is_private': 'Profilo privato?',
            'profile_pic': 'Immagine profilo',
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_person = True
        user.profile_pic = self.cleaned_data.get('profile_pic')
        user.save()
        person = Person.objects.create(user=user)
        person.libraries.add(self.cleaned_data.get('libraries'))
        person.libraries.add(Library.objects.get(name="Biblioteca Open"))
        person.save()
        membership = LibraryCards.objects.get(person_id=user.id, library=self.cleaned_data.get('libraries'))
        # membership.date_joined = datetime.date.today()
        membership.number_code = self.cleaned_data.get('library_card')
        membership.save()
        return user


class LibrarySignUpForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(LibrarySignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['city'].label = 'Città'

    name = forms.CharField(max_length=50, label="Nome")
    city = forms.ModelChoiceField(queryset=City.objects.all().order_by('name'))
    address = forms.CharField(max_length=50, label="Indirizzo")
    phone_number = forms.CharField(max_length=13, label="Telefono", required=True)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'password1', 'password2',
                  'name', 'city', 'address',
                  'email', 'phone_number', 'is_private',
                  'profile_pic')
        labels = {
            'city': 'Città',
            'address': 'Indirizzo',
            'phone_number': 'Telefono',
            'is_private': 'Profilo privato?',
            'profile_pic': 'Immagine profilo',
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_library = True
        user.profile_pic = self.cleaned_data.get('profile_pic')
        user.save()
        library = Library.objects.create(user=user,
                                         name=self.cleaned_data.get('name'),
                                         city=City.objects.get(name=self.cleaned_data.get('city')),
                                         address=self.cleaned_data.get('address'),
                                         phone_number=self.cleaned_data.get('phone_number'))
        library.save()
        return user


class ProfileUpdateForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'profile_update_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email', 'is_private', 'profile_pic')
        labels = {
            'is_private': 'Profilo privato?',
            'profile_pic': 'Immagine profilo',
        }


class LibraryProfileUpdateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LibraryProfileUpdateForm, self).__init__(*args, **kwargs)
        self.fields['city'].label = 'Città'

    helper = FormHelper()
    helper.form_id = 'library_profile_update_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Library
        fields = ('name', 'city',
                  'address', 'phone_number')
        labels = {
            'name': 'Nome biblioteca',
            'city': 'Città',
            'address': 'Indirizzo',
            'phone_number': 'Telefono',
        }


class LibraryCardsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LibraryCardsForm, self).__init__(*args, **kwargs)
        self.fields['library'].label = 'Biblioteca'

    helper = FormHelper()
    helper.form_id = 'library_cards_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = LibraryCards
        fields = ('library', 'number_code')
        labels = {
            'number_code': 'Tessera bibliotecaria',
        }


def form_validation_error(form):
    msg = ""
    for field in form:
        for error in field.errors:
            msg += "%s: %s \\n" % (field.label if hasattr(field, 'label') else 'Error', error)
    return msg
