import datetime

from django.test import TestCase
from django.urls import reverse

from accounts.models import User, Library, Person, LibraryCards
from digital_lending.models import Region, Province, City, Author, Category, Book, Publisher, Edition, Copy, Lending, \
    Reservation
from digital_lending.utils import copy_was_lent, check_person_active_book_reservations, check_available_reserved_books


class UtilsTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(email="test1@email.com", username="test1",
                                 first_name="alice", last_name="rossi",
                                 password="passwordtest1", is_private=False,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="test2@email.com", username="test2",
                                 first_name="bob", last_name="verdi",
                                 password="passwordtest2", is_private=True,
                                 is_person=True, is_library=False)
        User.objects.create_user(email="test3@email.com", username="bibliotest3",
                                 password="passwordtest3", is_private=False,
                                 is_person=False, is_library=True)
        User.objects.create_user(email="test4@email.com", username="Open",
                                 password="passwordtest4", is_private=False,
                                 is_person=False, is_library=True)

        Region.objects.create(name="Emilia Romagna")
        Province.objects.create(name="Modena", region_id=1)
        City.objects.create(name="Carpi", province_id=1)
        City.objects.create(name='Modena', province_id=1)
        Library.objects.create(user_id=3,
                               name="biblioteca3",
                               city_id=1)
        Library.objects.create(user_id=4,
                               name="Biblioteca Open",
                               city_id=2)

        Person.objects.create(user_id=1)
        Person.objects.create(user_id=2)

        LibraryCards.objects.create(person_id=1, library_id=3, number_code='DF3857485923')
        LibraryCards.objects.create(person_id=1, library_id=4, number_code='IT3823323233')
        LibraryCards.objects.create(person_id=2, library_id=3, number_code='JGJDJ9193922')

        Author.objects.create(name='Alessandro Manzoni')
        Category.objects.create(name='Narrativa')
        book = Book.objects.create(title='I Promessi Sposi')
        book.author.add(Author.objects.get(id=1))
        book.category.add(Category.objects.get(id=1))
        Publisher.objects.create(name='Mondadori', p_iva='12345678910')
        Edition.objects.create(isbn_digital='12345678901234567',
                               publisher_id=1, book_id=1, pub_date=datetime.date.today())
        Copy.objects.create(edition_id='12345678901234567', library_id=3, is_audiobook=False)

        lending = Lending.objects.create(person_id=1, copy_id=1,
                               date_lent=datetime.date.today())
        lending.date_returned = lending.compute_return_date()
        lending.save()

        Reservation.objects.create(person_id=2,
                                   edition_id='12345678901234567',
                                   date=datetime.date.today(), is_active=True)

    def test_copy_was_lent_returns_True(self):
        result = copy_was_lent(Copy.objects.get(id=1))

        self.assertTrue(result)

    def test_edition_detail_view_with_no_available_copies(self):
        self.client.login(
            username='test1',
            password='passwordtest1'
        )
        response = self.client.get(
            reverse('digital_lending:edition-detail', kwargs={'pk': '12345678901234567'})
        )
        self.assertContains(response, "Libro occupato")

        self.client.login(
            username='test2',
            password='passwordtest2'
        )
        response = self.client.get(
            reverse('digital_lending:edition-detail', kwargs={'pk': '12345678901234567'})
        )
        self.assertContains(response, "Libro occupato")

    def test_check_person_active_book_reservations(self):
        self.client.login(
            username='test2',
            password='passwordtest2'
        )

        self.assertEqual(Person.objects.get(user_id=2).reservations.all().count(), 1)
        self.assertEqual(len(check_person_active_book_reservations(User.objects.get(id=2))), 1)

    def test_check_available_reserved_books_is_empty(self):
        self.client.login(
            username='test2',
            password='passwordtest2'
        )

        self.assertEqual(len(check_available_reserved_books(User.objects.get(id=2))), 0)

    def test_check_available_reserved_books_is_not_empty(self):
        self.client.login(
            username='test2',
            password='passwordtest2'
        )

        Copy.objects.create(edition_id='12345678901234567', library_id=3, is_audiobook=False)

        self.assertEqual(len(check_available_reserved_books(User.objects.get(id=2))), 1)
        self.assertEqual(Lending.objects.all().count(), 2)






