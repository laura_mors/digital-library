from django.contrib import admin
from .models import *

@admin.register(Edition)
class EditionAdmin(admin.ModelAdmin):
    list_display = ("publisher", "book", "isbn_digital", 'isbn_physical', 'pub_date', 'language', 'copies')
    list_filter = ('book__category__name', 'publisher__name', 'language', )
    search_fields = ("book__title__icontains", "publisher__name__icontains",)

    def copies(self, obj):
        copies = Copy.objects.filter(edition=obj).count()
        return copies


@admin.register(Periodical)
class PeriodicalAdmin(admin.ModelAdmin):
    list_display = ("publisher", "category", "name", 'language', 'is_newspaper', 'issues')
    list_filter = ('publisher__name', 'category__name', 'language', )
    search_fields = ("name__icontains", "publisher__name__icontains",)

    def issues(self, obj):
        issues = Issue.objects.filter(periodical=obj).count()
        return issues


@admin.register(Lending)
class LendingAdmin(admin.ModelAdmin):
    list_display = ("person", "copy", "date_lent", "date_returned")
    list_filter = ("copy__edition__book__title", )
    search_fields = ("person__user__email__icontains", "person__user__username__icontains",
                     "copy__edition__book__title__icontains", )


@admin.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    list_display = ("person", "edition", "date", "is_active")
    list_filter = ("is_active", )
    search_fields = ("person__user__email__icontains", "person__user__username__icontains",
                     "edition__book__title__icontains", )


@admin.register(BookList)
class BookListAdmin(admin.ModelAdmin):
    list_display = ("owner", "title", "date_created", 'is_private', 'views')
    list_filter = ( 'owner__is_person', )
    search_fields = ("owner__username__icontains", "title__icontains",)


admin.site.register(Region)
admin.site.register(Province)
admin.site.register(City)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Book)
admin.site.register(Publisher)
admin.site.register(Copy)
admin.site.register(Issue)
admin.site.register(SavedList)
admin.site.register(Favorite)
admin.site.register(PurchaseSuggestion)
