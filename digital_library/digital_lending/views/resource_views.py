from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView
from accounts.decorators import library_required

from digital_lending.forms import *
from digital_lending.models import *
from digital_lending.utils import check_library_owned_copies, check_library_available_copies, get_lending_stats


class EntryCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = None
    template_name = ''
    form_class = None
    permission_denied_message = "Devi effettuare il login prima!"

    def handle_no_permission(self):
        messages.error(self.request, self.permission_denied_message)
        return super(self.__class__.__name__, self).handle_no_permission()


@method_decorator(library_required, name='dispatch')
class AuthorCreate(EntryCreate):
    model = Author
    template_name = 'digital_lending/resources/author/create.html'
    form_class = AuthorForm
    success_url = reverse_lazy('digital_lending:book-create')
    success_message = 'Autore creato con successo!'

    def get_context_data(self, **kwargs):
        context = super(AuthorCreate, self).get_context_data(**kwargs)
        context['profile_pk'] = self.request.user.id
        return context


@method_decorator(library_required, name='dispatch')
class AuthorUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Author
    template_name = 'digital_lending/resources/author/update.html'
    form_class = AuthorForm
    success_message = 'Autore aggiornato con successo!'

    def get_success_url(self):
        author_pk = self.object.pk
        return reverse_lazy('digital_lending:author-detail', kwargs={'pk': author_pk})


class AuthorDetail(DetailView):
    model = Author
    template_name = 'digital_lending/resources/author/detail.html'


class CategoryDetail(DetailView):
    model = Category
    template_name = 'digital_lending/resources/category/detail.html'


@method_decorator(library_required, name='dispatch')
class BookCreate(EntryCreate):
    model = Book
    template_name = 'digital_lending/resources/book/create.html'
    form_class = BookForm
    success_url = reverse_lazy('digital_lending:edition-create')
    success_message = 'Libro creato con successo!'

    def get_context_data(self, **kwargs):
        context = super(BookCreate, self).get_context_data(**kwargs)
        context['profile_pk'] = self.request.user.id
        return context


@method_decorator(library_required, name='dispatch')
class BookUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Book
    template_name = 'digital_lending/resources/book/update.html'
    form_class = BookForm
    success_message = 'Libro aggiornato con successo!'

    def get_success_url(self):
        book_pk = self.object.pk
        return reverse_lazy('digital_lending:book-detail', kwargs={'pk': book_pk})


class BookDetail(DetailView):
    model = Book
    template_name = 'digital_lending/resources/book/detail.html'


@method_decorator(library_required, name='dispatch')
class PublisherCreate(EntryCreate):
    model = Publisher
    template_name = 'digital_lending/resources/publisher/create.html'
    form_class = PublisherForm
    success_url = reverse_lazy('digital_lending:edition-create')
    success_message = 'Editore creato con successo!'

    def get_context_data(self, **kwargs):
        context = super(PublisherCreate, self).get_context_data(**kwargs)
        context['profile_pk'] = self.request.user.id
        return context


@method_decorator(library_required, name='dispatch')
class PublisherUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Publisher
    template_name = 'digital_lending/resources/publisher/update.html'
    form_class = PublisherForm
    success_message = 'Editore aggiornato con successo!'

    def get_success_url(self):
        publisher_pk = self.object.pk
        return reverse_lazy('digital_lending:publisher-detail', kwargs={'pk': publisher_pk})


class PublisherDetail(DetailView):
    model = Publisher
    template_name = 'digital_lending/resources/publisher/detail.html'


@method_decorator(library_required, name='dispatch')
class EditionCreate(EntryCreate):
    model = Edition
    template_name = 'digital_lending/resources/edition/create.html'
    form_class = EditionForm
    success_message = 'Edizione creata con successo!'

    def get_success_url(self):
        pk = self.object.isbn_digital
        return reverse_lazy('digital_lending:copy-create', kwargs={'pk': pk})


@method_decorator(library_required, name='dispatch')
class EditionUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Edition
    template_name = 'digital_lending/resources/edition/update.html'
    form_class = EditionForm
    success_message = 'Edizione aggiornata con successo!'

    def get_success_url(self):
        edition_pk = self.object.isbn_digital
        return reverse_lazy('digital_lending:edition-detail', kwargs={'pk': edition_pk})



class EditionDetail(DetailView):
    model = Edition
    template_name = 'digital_lending/resources/edition/detail.html'

    def get_context_data(self, **kwargs):
        context = super(EditionDetail, self).get_context_data(**kwargs)

        week_stats, month_stats = get_lending_stats(self.kwargs['pk'])
        context['week_stats'] = week_stats
        context['month_stats'] = month_stats

        context['favorites'] = Edition.objects.get(isbn_digital=self.kwargs['pk']).total_favorites.count()

        context['lists'] = Edition.objects.get(isbn_digital=self.kwargs['pk']).in_lists.all().count()
        context['has_ebooks'] = Copy.objects.filter(edition_id=self.kwargs['pk']).filter(is_audiobook=False)
        context['has_audiobooks'] = Copy.objects.filter(edition_id=self.kwargs['pk']).filter(is_audiobook=True)

        if self.request.user.is_authenticated and self.request.user.is_person:
            context['user_favorites'] = Person.objects.get(user_id=self.request.user.id).favorites.values_list(
                'edition_id', flat=True)

            if check_library_owned_copies(self.request.user, self.kwargs['pk']):
                context['library_has_copies'] = True
                available_copies = check_library_available_copies(self.request.user, self.kwargs['pk'])
                if available_copies:
                    context['library_has_available_copies'] = True
                else:
                    context['library_has_available_copies'] = False
            else:
                context['library_has_copies'] = False

                purchase_suggestions = PurchaseSuggestion.objects.filter(person_id=self.request.user.id,
                                                                        edition_id=self.kwargs['pk'])
                if purchase_suggestions:
                    context['user_suggested_purchase'] = True
                else:
                    context['user_suggested_purchase'] = False

            try:
                Lending.objects.get(person=Person.objects.get(user_id=self.request.user.id),
                                    copy__in=Edition.objects.get(isbn_digital=self.kwargs['pk']).copies.all(),
                                    date_returned__gte=datetime.date.today())
                context['user_has_book'] = True
            except Lending.DoesNotExist:
                context['user_has_book'] = False

            try:
                Reservation.objects.get(person=Person.objects.get(user_id=self.request.user.id),
                                        edition=Edition.objects.get(isbn_digital=self.kwargs['pk']),
                                        is_active=True)
                context['user_made_reservation'] = True
            except Reservation.DoesNotExist:
                context['user_made_reservation'] = False

        return context


class EditionList(ListView):
    paginate_by = 6
    model = Edition
    template_name = 'digital_lending/resources/edition/list.html'

    def get_context_data(self, **kwargs):
        context = super(EditionList, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['date'] = self.request.GET.get("date")
        context['category'] = self.request.GET.get('category')
        context['availability'] = self.request.GET.get("availability")
        context['type'] = self.request.GET.get("type")
        context['sort'] = self.request.GET.get("sort")
        return context

    def get_queryset(self):
        object_list = Edition.objects.all().order_by('-pub_date')

        query = self.request.GET.get('q')
        if query:
            object_list = object_list.filter(
                Q(book__title__icontains=query) | Q(book__author__name__icontains=query)
            )

        date = self.request.GET.get('date')
        if date == "last_7_days":
            filter_date = datetime.date.today() - datetime.timedelta(days=7)
            object_list = object_list.filter(pub_date__gte=filter_date)
        elif date == "last_15_days":
            filter_date = datetime.date.today() - datetime.timedelta(days=15)
            object_list = object_list.filter(pub_date__gte=filter_date)
        elif date == "last_month":
            filter_date = datetime.date.today() - datetime.timedelta(days=30)
            object_list = object_list.filter(pub_date__gte=filter_date)

        category = self.request.GET.get('category')
        if category in Category.objects.all().values_list('name', flat=True):
            object_list = object_list.filter(book__category__name__icontains=category)

        availability = self.request.GET.get('availability')
        if availability == "available":
            available_editions = []
            for edition in Edition.objects.all():
                available_copies = check_library_available_copies(self.request.user, edition.isbn_digital)
                if available_copies:
                    available_editions.append(available_copies[0])
            object_list = object_list.filter(copies__in=available_editions).distinct()

        type = self.request.GET.get('type')
        if type == "library":
            object_list = object_list.exclude(copies__library__name="Biblioteca Open").distinct()
        elif type == "open":
            object_list = object_list.filter(copies__library__name="Biblioteca Open").distinct()

        order = self.request.GET.get("sort")
        if order == "oldest":
            object_list = object_list.order_by("pub_date")
        elif order == "title_asc":
            object_list = object_list.order_by("-book__title")
        elif order == "title_desc":
            object_list = object_list.order_by("book__title")
        else:
            object_list = object_list.order_by("-pub_date")

        return object_list


@method_decorator(library_required, name='dispatch')
class LibraryEditionsList(ListView):
    paginate_by = 6
    model = Edition
    template_name = 'digital_lending/resources/edition/list.html'

    def get_queryset(self):
        library_copies = Copy.objects.filter(library_id=self.request.user.id)
        library_editions = Edition.objects.filter(copies__in=library_copies).distinct()
        return library_editions


@method_decorator(library_required, name='dispatch')
class CopyCreate(EntryCreate):
    model = Copy
    template_name = 'digital_lending/resources/edition/copy/create.html'
    form_class = CopyForm
    success_message = 'Copia creata con successo!'

    def get_success_url(self):
        edition_pk = self.object.edition.isbn_digital
        return reverse_lazy('digital_lending:copies', kwargs={'pk': edition_pk})

    def get_context_data(self, **kwargs):
        context = super(CopyCreate, self).get_context_data(**kwargs)
        context['edition_pk'] = self.kwargs['pk']
        return context

    def form_valid(self, form):
        form.instance.library = Library.objects.get(user_id=self.request.user.id)
        form.instance.edition = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return super().form_valid(form)


@method_decorator(library_required, name='dispatch')
class CopyUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Copy
    template_name = 'digital_lending/resources/edition/copy/update.html'
    form_class = CopyUpdateForm
    success_message = 'Copia aggiornata con successo!'

    def get_success_url(self):
        edition_pk = self.object.edition.isbn_digital
        return reverse_lazy('digital_lending:copies', kwargs={'pk': edition_pk})

    def get_object(self, *args, **kwargs):
        obj = super(CopyUpdate, self).get_object(*args, **kwargs)
        if not obj.library_id == self.request.user.id:
            raise PermissionDenied
        return obj


@method_decorator(library_required, name='dispatch')
class CopyDelete(LoginRequiredMixin, DeleteView):
    model = Copy
    template_name = 'digital_lending/resources/edition/copy/delete.html'
    success_message = 'Copia eliminata con successo!'

    def get_success_url(self):
        edition_pk = self.object.edition.isbn_digital
        return reverse_lazy('digital_lending:copies', kwargs={'pk': edition_pk})

    def delete(self, request, *args, **kwargs):
        obj = super(CopyDelete, self).delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return obj

    def get_object(self, *args, **kwargs):
        obj = super(CopyDelete, self).get_object(*args, **kwargs)
        if not obj.library_id == self.request.user.id:
            raise PermissionDenied
        return obj


@method_decorator(library_required, name='dispatch')
class CopyList(ListView):
    paginate_by = 6
    model = Copy
    template_name = 'digital_lending/resources/edition/copy/list.html'

    def get_context_data(self, **kwargs):
        context = super(CopyList, self).get_context_data(**kwargs)
        context['edition'] = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return context

    def get_queryset(self):
        return Copy.objects.filter(library_id=self.request.user.id).filter(edition_id=self.kwargs['pk'])


@method_decorator(library_required, name='dispatch')
class PeriodicalCreate(EntryCreate):
    model = Periodical
    template_name = 'digital_lending/resources/periodical/create.html'
    form_class = PeriodicalForm
    success_message = 'Periodico creato con successo!'

    def get_success_url(self):
        pk = self.object.id
        return reverse_lazy('digital_lending:issue-create', kwargs={'pk': pk})


@method_decorator(library_required, name='dispatch')
class PeriodicalUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Periodical
    template_name = 'digital_lending/resources/periodical/update.html'
    form_class = PeriodicalForm
    success_message = 'Periodico aggiornato correttamente!'

    def get_success_url(self):
        pk = self.object.id
        return reverse_lazy('digital_lending:periodical-issues', kwargs={'pk': pk})


class PeriodicalList(ListView):
    paginate_by = 6
    model = Periodical
    template_name = 'digital_lending/resources/periodical/list.html'

    def get_context_data(self, **kwargs):
        context = super(PeriodicalList, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['type'] = self.request.GET.get('type')
        context['date'] = self.request.GET.get('date')
        context['category'] = self.request.GET.get('category')
        context['sort'] = self.request.GET.get('sort')
        return context

    def get_queryset(self):
        object_list = Periodical.objects.all().order_by('name')

        query = self.request.GET.get('q')
        if query:
            object_list = object_list.filter(name__icontains=query)

        type = self.request.GET.get('type')
        if type == "newspaper":
            object_list = object_list.filter(is_newspaper=True)
        elif type == "magazine":
            object_list = object_list.filter(is_newspaper=False)

        date = self.request.GET.get('date')
        if date == "last_7_days":
            added_issues = Issue.objects.filter(pub_date__gte=datetime.date.today() - datetime.timedelta(days=7))
            object_list = object_list.filter(issues__in=added_issues).distinct()
        elif date == "last_15_days":
            added_issues = Issue.objects.filter(pub_date__gte=datetime.date.today() - datetime.timedelta(days=15))
            object_list = object_list.filter(issues__in=added_issues).distinct()
        elif date == "last_month":
            added_issues = Issue.objects.filter(pub_date__month=datetime.date.today().month)
            object_list = object_list.filter(issues__in=added_issues).distinct()


        category = self.request.GET.get('category')
        if category in Category.objects.all().values_list('name', flat=True):
            object_list = object_list.filter(category__name=category)

        order = self.request.GET.get("sort")
        if order == "title_asc":
            object_list = object_list.order_by("name")
        elif order == "title_desc":
            object_list = object_list.order_by("-name")

        return object_list


@method_decorator(library_required, name='dispatch')
class PeriodicalIssueCreate(EntryCreate):
    model = Issue
    template_name = 'digital_lending/resources/periodical/issue/create.html'
    form_class = PeriodicalIssueForm
    success_message = 'Numero creato con successo!'

    def get_success_url(self):
        pk = self.object.periodical.id
        return reverse_lazy('digital_lending:periodical-issues', kwargs={'pk': pk})

    def form_valid(self, form):
        form.instance.periodical = Periodical.objects.get(id=self.kwargs['pk'])
        form.instance.library = Library.objects.get(user_id=self.request.user.id)
        return super().form_valid(form)


@method_decorator(library_required, name='dispatch')
class PeriodicalIssueUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Issue
    template_name = 'digital_lending/resources/periodical/issue/update.html'
    form_class = PeriodicalIssueForm
    success_message = 'Numero aggiornato con successo!'

    def get_success_url(self):
        pk = self.object.periodical.id
        return reverse_lazy('digital_lending:periodical-issues', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(PeriodicalIssueUpdate, self).get_object(*args, **kwargs)
        if not obj.library_id == self.request.user.id:
            raise PermissionDenied
        return obj


@method_decorator(library_required, name='dispatch')
class PeriodicalIssueDelete(LoginRequiredMixin, DeleteView):
    model = Issue
    template_name = 'digital_lending/resources/periodical/issue/delete.html'
    success_message = 'Numero eliminato con successo!'

    def get_success_url(self):
        pk = self.object.periodical.id
        return reverse_lazy('digital_lending:periodical-issues', kwargs={'pk': pk})

    def delete(self, request, *args, **kwargs):
        obj = super(PeriodicalIssueDelete, self).delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return obj

    def get_object(self, *args, **kwargs):
        obj = super(PeriodicalIssueDelete, self).get_object(*args, **kwargs)
        if not obj.library_id == self.request.user.id:
            raise PermissionDenied
        return obj


class PeriodicalIssueList(ListView):
    paginate_by = 6
    model = Issue
    template_name = 'digital_lending/resources/periodical/issue/list.html'

    def get_context_data(self, **kwargs):
        context = super(PeriodicalIssueList, self).get_context_data(**kwargs)
        context['periodical'] = Periodical.objects.get(id=self.kwargs['pk'])
        context['sort'] = self.request.GET.get('sort')
        if self.request.user.is_authenticated and self.request.user.is_person:
            user_libraries = Person.objects.get(user_id=self.request.user.id).libraries.all()
            context['user_libraries'] = user_libraries
        return context

    def get_queryset(self):
        object_list = Periodical.objects.get(id=self.kwargs['pk']).issues.order_by('-pub_date')

        order = self.request.GET.get("sort")
        if order == "oldest":
            object_list = object_list.order_by("pub_date")
        else:
            object_list = object_list.order_by("-pub_date")

        return object_list


@method_decorator(library_required, name='dispatch')
class LibraryPeriodicalsList(ListView):
    paginate_by = 6
    model = Periodical
    template_name = 'digital_lending/resources/periodical/list.html'

    def get_queryset(self):
        library_issues = Issue.objects.filter(library_id=self.request.user.id)
        library_periodicals = Periodical.objects.filter(issues__in=library_issues).distinct()
        return library_periodicals.order_by('name')
