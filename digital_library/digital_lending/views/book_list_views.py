from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView, UpdateView
from django.views import View

from digital_lending.forms import *
from digital_lending.models import *
from digital_lending.utils import increment_list_view_count

from .resource_views import EntryCreate


class BookListCreate(EntryCreate):
    model = BookList
    template_name = 'digital_lending/resources/book_list/create.html'
    form_class = BookListForm
    success_url = reverse_lazy('digital_lending:my-book-lists')
    success_message = "Lista creata con successo!"

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class UserBookLists(ListView):
    paginate_by = 6
    model = BookList
    template_name = 'digital_lending/resources/book_list/list.html'

    def get_queryset(self):
        return BookList.objects.filter(owner=User.objects.get(id=self.request.user.id)).order_by('id')


class UserSavedBookLists(ListView):
    paginate_by = 6
    model = SavedList
    template_name = 'digital_lending/resources/book_list/saved.html'

    def get_queryset(self):
        return SavedList.objects.filter(user=User.objects.get(id=self.request.user.id)).order_by('-date_saved')


class BookLists(ListView):
    paginate_by = 6
    model = BookList
    template_name = 'digital_lending/resources/book_list/list.html'

    def get_context_data(self, **kwargs):
        context = super(BookLists, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            user_saved_lists = SavedList.objects.filter(user=self.request.user).values_list('list_id', flat=True)
            context['user_saved_lists'] = user_saved_lists
        context['sort'] = self.request.GET.get('sort')
        return context

    def get_queryset(self):
        object_list = BookList.objects.filter(is_private=False)

        query = self.request.GET.get('q')
        if query:
            object_list = object_list.filter(title__icontains=query)

        edition = self.request.GET.get('edition')
        if edition:
            edition_in_lists = EditionInList.objects.filter(edition_id=edition)
            object_list = object_list.filter(id__in=edition_in_lists.values_list('list_id')).order_by('-date_created')

        order = self.request.GET.get("sort")
        if order == "oldest":
            object_list = object_list.order_by("date_created")
        else:
            object_list = object_list.order_by("-date_created")

        return object_list


class BookListDetail(DetailView):
    model = BookList
    template_name = 'digital_lending/resources/book_list/detail.html'

    def get(self, request, *args, **kwargs):
        increment_list_view_count(self.kwargs['pk'])
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BookListDetail, self).get_context_data(**kwargs)
        context['list'] = BookList.objects.get(id=self.kwargs['pk'])
        context['views'] = BookList.objects.get(id=self.kwargs['pk']).views

        order = self.request.GET.get("sort")

        if order == "oldest":
            context['editions'] = BookList.objects.get(id=self.kwargs['pk']).books.all().order_by("date_added")
        elif order == "title_asc":
            context['editions'] = BookList.objects.get(id=self.kwargs['pk']).books.all().order_by("-edition__book__title")
        elif order == "title_desc":
            context['editions'] = BookList.objects.get(id=self.kwargs['pk']).books.all().order_by("edition__book__title")
        else:
            context['editions'] = BookList.objects.get(id=self.kwargs['pk']).books.all().order_by("-date_added")
        context['sort'] = self.request.GET.get('sort')
        return context


class BookListUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = BookList
    template_name = 'digital_lending/resources/book_list/update.html'
    success_url = reverse_lazy('digital_lending:my-book-lists')
    form_class = BookListForm
    success_message = 'Lista aggiornata con successo!'

    def get_object(self, *args, **kwargs):
        obj = super(BookListUpdate, self).get_object(*args, **kwargs)
        if not obj.owner_id == self.request.user.id:
            raise PermissionDenied
        return obj


class BookListDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = BookList
    template_name = 'digital_lending/resources/book_list/delete.html'
    success_url = reverse_lazy('digital_lending:my-book-lists')
    success_message = 'Lista eliminata con successo!'

    def get_object(self, *args, **kwargs):
        obj = super(BookListDelete, self).get_object(*args, **kwargs)
        if not obj.owner_id == self.request.user.id:
            raise PermissionDenied
        return obj


class AddBookToListView(EntryCreate):
    model = EditionInList
    template_name = 'digital_lending/resources/book_list/edition_entry/create.html'
    form_class = AddBookToListForm
    success_message = 'Il libro è stato aggiunto alla lista!'

    def get_success_url(self):
        pk = self.object.edition.isbn_digital
        return reverse_lazy('digital_lending:edition-detail', kwargs={'pk': pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddBookToListView, self).get_context_data(**kwargs)
        context['edition'] = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.edition = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        try:
            return super().post(request, *args, **kwargs)
        except IntegrityError:
            messages.add_message(request, messages.ERROR,
                                 'Hai già inserito questo libro nella lista.')
            return render(request, template_name=self.template_name, context=self.get_context_data())


class DeleteBookInList(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = EditionInList
    template_name = 'digital_lending/resources/book_list/edition_entry/delete.html'
    success_message = 'Il libro è stato eliminato dalla lista!'

    def get_success_url(self):
        pk = self.object.list.id
        return reverse_lazy('digital_lending:book-list-detail', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(DeleteBookInList, self).get_object(*args, **kwargs)
        if not obj.list.owner_id == self.request.user.id:
            raise PermissionDenied
        return obj


class SaveBookListView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        obj, created = SavedList.objects.get_or_create(
            user=User.objects.get(id=self.request.user.id),
            list=BookList.objects.get(id=self.kwargs['pk']))
        if not created:
            obj.delete()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))