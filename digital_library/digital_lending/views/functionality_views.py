from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import DeleteView, ListView
from accounts.decorators import person_required, library_required
from django.views import View

from digital_lending.models import *
from digital_lending.utils import check_library_available_copies


@method_decorator(person_required, name='dispatch')
class ChooseCopyToLendView(LoginRequiredMixin, ListView):
    paginate_by = 6
    model = Copy
    template_name = 'digital_lending/resources/lending/available_copies_list.html'

    def get_queryset(self):
        available_copies = check_library_available_copies(self.request.user, self.kwargs['pk'])
        try:
            Lending.objects.get(person=Person.objects.get(user_id=self.request.user.id),
                                copy__in=Edition.objects.get(isbn_digital=self.kwargs['pk']).copies.all(),
                                date_returned__gte=datetime.date.today())
            available_copies = []
        except Lending.DoesNotExist:
            pass
        return available_copies


@method_decorator(person_required, name='dispatch')
class LendBookView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        copy = Copy.objects.get(id=self.kwargs['pk'])
        lending = Lending(person=Person.objects.get(user_id=request.user.id),
                          copy=copy,
                          date_lent=datetime.date.today())
        lending.date_returned = lending.compute_return_date()
        lending.save()
        try:
            reservation = Reservation.objects.get(
                person=Person.objects.get(user_id=request.user.id),
                edition_id=copy.edition.isbn_digital, is_active=True)
            reservation.is_active = False
            reservation.save()
        except Reservation.DoesNotExist:
            pass

        return HttpResponseRedirect(reverse_lazy('digital_lending:book-history'))

class PersonBookHistoryList(ListView):
    paginate_by = 6
    model = Lending
    template_name = 'digital_lending/resources/lending/list.html'

    def get_queryset(self):
        book_history = Lending.objects.filter(person_id=self.request.user.id).order_by('-date_lent')
        return book_history


@method_decorator(person_required, name='dispatch')
class BookReservationView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        obj, created = Reservation.objects.get_or_create(
            person=Person.objects.get(user_id=self.request.user.id),
            edition=Edition.objects.get(isbn_digital=self.kwargs['pk']),
            date=datetime.date.today(),
            is_active=True)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class PersonReservationList(ListView):
    paginate_by = 6
    model = Reservation
    template_name = 'digital_lending/resources/reservation/list.html'

    def get_context_data(self, **kwargs):
        context = super(PersonReservationList, self).get_context_data(**kwargs)
        #context['available_reserved_books'] = check_person_book_reservations(self)
        return context

    def get_queryset(self):
        reservations = Reservation.objects.filter(person_id=self.request.user.id).filter(
            is_active=True).order_by('-date')
        return reservations


@method_decorator(person_required, name='dispatch')
class ReservationDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Reservation
    template_name = 'digital_lending/resources/reservation/delete.html'
    success_url = reverse_lazy('digital_lending:book-reservations')
    success_message = 'Prenotazione eliminata con successo!'

    def get_object(self, *args, **kwargs):
        obj = super(ReservationDelete, self).get_object(*args, **kwargs)
        if not obj.person_id == self.request.user.id:
            raise PermissionDenied
        return obj



@method_decorator(person_required, name='dispatch')
class BookFavoriteView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        obj, created = Favorite.objects.get_or_create(
            person=Person.objects.get(user_id=self.request.user.id),
            edition=Edition.objects.get(isbn_digital=self.kwargs['pk']))
        if not created:
            obj.delete()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class PersonFavoriteBooksList(ListView):
    paginate_by = 6
    model = Favorite
    template_name = 'digital_lending/resources/favorite/list.html'

    def get_context_data(self, **kwargs):
        context = super(PersonFavoriteBooksList, self).get_context_data(**kwargs)
        context['sort'] = self.request.GET.get("sort")
        return context

    def get_queryset(self):
        favorites = Favorite.objects.filter(person_id=self.request.user.id).order_by('-date')

        order = self.request.GET.get("sort")
        if order == "oldest":
            favorites = favorites.order_by("date")
        elif order == "title_asc":
            favorites = favorites.order_by("-edition__book__title")
        elif order == "title_desc":
            favorites = favorites.order_by("edition__book__title")
        else:
            favorites = favorites.order_by("-date")

        return favorites


@method_decorator(person_required, name='dispatch')
class FavoriteBookDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Favorite
    template_name = 'digital_lending/resources/favorite/delete.html'
    success_url = reverse_lazy('digital_lending:favorite-books')
    success_message = 'Il libro è stato eliminato dai preferiti!'

    def get_object(self, *args, **kwargs):
        obj = super(FavoriteBookDelete, self).get_object(*args, **kwargs)
        if not obj.person_id == self.request.user.id:
            raise PermissionDenied
        return obj


@method_decorator(person_required, name='dispatch')
class SuggestPurchaseView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        user = Person.objects.get(user_id=self.request.user.id)
        for library in user.libraries.all():
            obj, created = PurchaseSuggestion.objects.get_or_create(
                person=user,
                edition=Edition.objects.get(isbn_digital=self.kwargs['pk']),
                library=library)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@method_decorator(library_required, name='dispatch')
class LibraryPurchaseSuggestionsList(ListView):
    paginate_by = 6
    model = PurchaseSuggestion
    template_name = 'digital_lending/resources/purchase_suggestion/list.html'

    def get_queryset(self):
        suggestions = PurchaseSuggestion.objects.filter(
            library_id=self.request.user.id).values('edition').annotate(dcount=Count('edition')).order_by('-date')
        return suggestions






