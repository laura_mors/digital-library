# Generated by Django 3.2.4 on 2021-07-19 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AddedIn',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='Copy',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('city', models.CharField(blank=True, max_length=50, null=True)),
                ('address', models.CharField(blank=True, max_length=50, null=True)),
                ('email', models.CharField(blank=True, max_length=254, null=True)),
                ('p_iva', models.CharField(max_length=11)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='x', max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Province',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='x', max_length=50)),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='provinces', to='digital_lending.region')),
            ],
        ),
        migrations.CreateModel(
            name='LibraryCards',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_joined', models.DateField(blank=True, null=True)),
                ('library_card', models.CharField(blank=True, max_length=7, null=True)),
                ('library', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='library_cards_libraries', to='accounts.library')),
                ('person', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='library_cards_people', to='accounts.person')),
            ],
            options={
                'verbose_name_plural': 'Library cards',
            },
        ),
        migrations.CreateModel(
            name='Lending',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('copy', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lendings', to='digital_lending.copy')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='borrowings', to='accounts.person')),
            ],
        ),
        migrations.CreateModel(
            name='Edition',
            fields=[
                ('isbn_digital', models.CharField(max_length=17, primary_key=True, serialize=False)),
                ('isbn_physical', models.CharField(max_length=17)),
                ('language', models.CharField(max_length=20)),
                ('publisher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='editions', to='digital_lending.publisher')),
            ],
        ),
        migrations.CreateModel(
            name='Ebook',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('format', models.CharField(max_length=10)),
                ('copy', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='ebooks', to='digital_lending.copy')),
            ],
        ),
        migrations.AddField(
            model_name='copy',
            name='edition',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='copies', to='digital_lending.edition'),
        ),
        migrations.AddField(
            model_name='copy',
            name='library',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='library', to='accounts.library'),
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('cap', models.CharField(max_length=5)),
                ('population', models.BigIntegerField()),
                ('province', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cities', to='digital_lending.province')),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('desc', models.TextField(max_length=50)),
                ('book', models.ManyToManyField(through='digital_lending.AddedIn', to='digital_lending.Book')),
            ],
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('birth_date', models.DateField()),
                ('birth_country', models.CharField(max_length=40)),
                ('birth_city', models.CharField(max_length=40)),
                ('book', models.ManyToManyField(related_name='author', to='digital_lending.Book')),
            ],
        ),
        migrations.CreateModel(
            name='AudioBook',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('format', models.CharField(max_length=10)),
                ('listening_type', models.CharField(max_length=9)),
                ('copy', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='audiobooks', to='digital_lending.copy')),
            ],
        ),
        migrations.AddField(
            model_name='addedin',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='categories', to='digital_lending.book'),
        ),
        migrations.AddField(
            model_name='addedin',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='books_in_category', to='digital_lending.category'),
        ),
    ]
