# Generated by Django 3.2.4 on 2021-09-12 15:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('digital_lending', '0055_auto_20210911_1540'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='city',
            name='Each city has to have a unique CAP',
        ),
    ]
