# Generated by Django 3.2.4 on 2021-09-15 11:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0022_alter_user_is_person'),
        ('digital_lending', '0060_auto_20210915_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='library',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='periodical_issues', to='accounts.library'),
        ),
    ]
