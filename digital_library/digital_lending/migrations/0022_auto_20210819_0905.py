# Generated by Django 3.2.4 on 2021-08-19 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('digital_lending', '0021_auto_20210819_0904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.ManyToManyField(blank=True, related_name='books', to='digital_lending.Author'),
        ),
        migrations.AlterField(
            model_name='book',
            name='category',
            field=models.ManyToManyField(blank=True, related_name='books', to='digital_lending.Category'),
        ),
    ]
