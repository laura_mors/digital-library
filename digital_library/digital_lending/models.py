import datetime

from django.core.validators import RegexValidator
from django.db import models


class Region(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class Province(models.Model):
    region = models.ForeignKey(Region, related_name="provinces", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class City(models.Model):
    province = models.ForeignKey(Province, related_name="cities", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    cap = models.CharField(validators=[RegexValidator(regex='^.{5}$',
                                                      message='Il CAP deve essere lungo 5 cifre')],
                           max_length=5)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Cities"


class Category(models.Model):
    name = models.CharField(max_length=40, verbose_name='nome categoria')
    desc = models.TextField(max_length=300, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"


class Author(models.Model):
    name = models.CharField(max_length=60)
    date_of_birth = models.DateField(blank=True, null=True)
    date_of_death = models.DateField(blank=True, null=True)
    birth_country = models.CharField(max_length=40, blank=True, null=True)
    birth_city = models.CharField(max_length=40, blank=True, null=True)
    short_bio = models.TextField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=80, verbose_name='titolo libro')
    author = models.ManyToManyField(Author, related_name='books')
    category = models.ManyToManyField(Category, related_name='books')

    def __str__(self):
        return self.title


class Publisher(models.Model):
    name = models.CharField(max_length=50, verbose_name='nome editore')
    city = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=254, blank=True, null=True)
    phone_number = models.CharField(max_length=11, blank=True, null=True)
    p_iva = models.CharField(validators=[RegexValidator(regex='^.{11}$',
                                                        message='La partita IVA deve essere lunga 11 cifre.')],
                             max_length=11)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'p_iva'],
                                    name='Each publisher has to have a unique p. IVA')
        ]


class Edition(models.Model):
    publisher = models.ForeignKey(Publisher, related_name="editions", on_delete=models.CASCADE)
    book = models.ForeignKey(Book, related_name="editions", on_delete=models.CASCADE)
    isbn_digital = models.CharField(validators=[RegexValidator(regex='^.{13}$',
                                                               message='Il codice ISBN deve essere lungo 13 cifre')],
                                    max_length=13, primary_key=True)
    isbn_physical = models.CharField(validators=[RegexValidator(regex='^.{13}$',
                                                                message='Il codice ISBN deve essere lungo 13 cifre')],
                                     max_length=13, blank=True, null=True)
    pub_date = models.DateField(editable=True)
    language = models.CharField(max_length=20, blank=True, null=True, verbose_name='lingua')
    pic = models.ImageField(upload_to='images/', blank=True, null=True)
    summary = models.TextField(max_length=3000, blank=True, null=True)

    def __str__(self):
        return self.book.title


from accounts.models import Library, User


class Copy(models.Model):
    edition = models.ForeignKey(Edition, related_name='copies', on_delete=models.CASCADE)
    library = models.ForeignKey(Library, related_name='copies', on_delete=models.CASCADE)
    date_added = models.DateField(auto_now_add=True)
    is_audiobook = models.BooleanField(default=False)
    file = models.FileField(upload_to='uploads/', blank=True, null=True)

    def __str__(self):
        return self.edition.book.title

    class Meta:
        verbose_name_plural = "Copies"


class Periodical(models.Model):
    publisher = models.ForeignKey(Publisher, related_name="periodicals", on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name="periodicals", on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    language = models.CharField(max_length=20, blank=True, null=True, verbose_name='lingua')
    is_newspaper = models.BooleanField(default=False)
    pic = models.ImageField(upload_to='images/', blank=True, null=True)

    def __str__(self):
        return self.name


class Issue(models.Model):
    periodical = models.ForeignKey(Periodical, related_name="issues", on_delete=models.CASCADE)
    library = models.ForeignKey(Library, related_name='periodical_issues', on_delete=models.CASCADE)
    number = models.IntegerField(editable=True)
    pub_date = models.DateField(editable=True)
    file = models.FileField(upload_to='uploads/', blank=True, null=True)

    def __str__(self):
        return self.periodical.name + ' n. ' + str(self.number)


from accounts.models import Person


class Lending(models.Model):
    person = models.ForeignKey(Person, related_name='borrowed', on_delete=models.CASCADE)
    copy = models.ForeignKey(Copy, related_name='lent_to', on_delete=models.CASCADE)
    date_lent = models.DateField(auto_now_add=True)
    date_returned = models.DateField(blank=True, null=True)

    def compute_return_date(self):
        return self.date_lent + datetime.timedelta(days=14)

    def is_lending_active(self):
        return self.date_returned > datetime.date.today()

    def __str__(self):
        return self.person.user.username + ' borrowed ' + self.copy.edition.book.title


class BookList(models.Model):
    owner = models.ForeignKey(User, related_name="lists", on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    desc = models.TextField(max_length=500, default='', blank=True, null=True)
    date_created = models.DateField(auto_now_add=True)
    is_private = models.BooleanField(default=True)
    views = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return self.title


class EditionInList(models.Model):
    edition = models.ForeignKey(Edition, related_name="in_lists", on_delete=models.CASCADE)
    list = models.ForeignKey(BookList, related_name="books", on_delete=models.CASCADE)
    date_added = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.edition.book.title

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['edition', 'list'],
                                    name='A certain edition of a a book cannot appear more than once in the same list')
        ]


class SavedList(models.Model):
    list = models.ForeignKey(BookList, related_name="saved_by", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="lists_saved", on_delete=models.CASCADE)
    date_saved = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.list.title + ' saved by ' + self.user.username


class Reservation(models.Model):
    person = models.ForeignKey(Person, related_name='reservations', on_delete=models.CASCADE)
    edition = models.ForeignKey(Edition, related_name='reservations', on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    is_active = models.BooleanField(default=True, verbose_name='prenotazione attiva')

    def get_reservation_queue(self):
        return Reservation.objects.filter(edition=self).filter(is_active=True).order_by('date')

    def __str__(self):
        return self.person.user.username + ' reserved ' + self.edition.book.title


class Favorite(models.Model):
    person = models.ForeignKey(Person, related_name='favorites', on_delete=models.CASCADE)
    edition = models.ForeignKey(Edition, related_name='total_favorites', on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.person.user.username + ' likes ' + self.edition.book.title


class PurchaseSuggestion(models.Model):
    person = models.ForeignKey(Person, related_name='suggested_purchases', on_delete=models.CASCADE)
    edition = models.ForeignKey(Edition, related_name='purchase_suggestions', on_delete=models.CASCADE)
    library = models.ForeignKey(Library, related_name='purchase_suggestions', on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.person.user.username + ' ha suggerito di acquistare ' + self.edition.book.title
