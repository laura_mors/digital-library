from django.db.models import F
from django.shortcuts import get_object_or_404

from accounts.models import Person, User
from digital_lending.models import Edition, Lending, BookList, Reservation, Copy

import datetime


def lend_reserved_book(user, copy):
    lending, created = Lending.objects.get_or_create(person_id=user.id,
                                            copy_id=copy.id,
                                            date_lent=datetime.date.today())
    lending.date_returned = lending.compute_return_date()
    lending.save()

    reservation = Reservation.objects.get(
        person_id=user.id,
        edition_id=copy.edition_id, is_active=True)
    reservation.is_active = False
    reservation.save()


def check_person_active_book_reservations(user):
    active_reservations = Reservation.objects.filter(
        person=Person.objects.get(
            user_id=user.id)).filter(
        is_active=True)

    return active_reservations


def check_available_reserved_books(user):
    active_reservations = check_person_active_book_reservations(user)
    available_books = []

    if active_reservations:
        for reservation in active_reservations:
            edition = reservation.edition
            available_copies = check_library_available_copies(user, edition.isbn_digital)
            if available_copies:
                reservation_queue = Reservation.get_reservation_queue(edition)
                if reservation_queue.first().person.user_id == user.id:
                    available_books.append(edition)
                    lend_reserved_book(user, available_copies[0])

    return available_books


def copy_was_lent(copy):
    if copy.library.user == User.objects.get(username="Open"):
        return False
    else:
        try:
            Lending.objects.filter(date_returned__gte=datetime.date.today()).get(copy=copy)
            return True
        except Lending.DoesNotExist:
            return False


def check_library_owned_copies(user, edition_id):
    book_edition = get_object_or_404(Edition, isbn_digital=edition_id)
    copies = book_edition.copies.all()
    if user.is_authenticated and user.is_person:
        user_libraries = Person.objects.get(user_id=user.id).libraries.all()
    else:
        user_libraries = []
    library_copies = [copy for copy in copies if copy.library in user_libraries]

    return library_copies


def check_library_available_copies(user, edition_id):
    library_copies = check_library_owned_copies(user, edition_id)
    copies_available = [copy for copy in library_copies if not copy_was_lent(copy)]

    return copies_available


def increment_list_view_count(list_id):
    list, created = BookList.objects.get_or_create(id=list_id)
    list.views = F("views") + 1
    list.save()


def get_lending_stats(edition_id):
    edition = Edition.objects.get(isbn_digital=edition_id)
    last_7_days = datetime.date.today() - datetime.timedelta(days=7)
    times_lent_last_7_days = Lending.objects.filter(copy__edition=edition,
                                                    date_lent__range=[last_7_days, datetime.date.today()]).all().count()
    times_lent_this_month = Lending.objects.filter(copy__edition=edition,
                                                   date_lent__month=datetime.date.today().month).all().count()
    return times_lent_last_7_days, times_lent_this_month
