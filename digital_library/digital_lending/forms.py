import datetime

from django import forms
from django.db import transaction

from digital_lending.models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class AuthorForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'author_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Author
        fields = '__all__'
        labels = {
            'name': 'Nome',
            'date_of_birth': 'Data di nascita',
            'date_of_death': 'Data di morte',
            'birth_country': 'Paese di nascita',
            'birth_city': 'Città di nascita',
            'short_bio': 'Breve biografia'
        }


class CategoryForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'category_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Category
        fields = '__all__'


class BookForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.fields['author'].label = 'Autore'
        self.fields['category'].label = 'Categoria'

    helper = FormHelper()
    helper.form_id = 'book_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Book
        fields = '__all__'
        labels = {
            'title': 'Titolo',
        }


class PublisherForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'publisher_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Publisher
        fields = '__all__'
        labels = {
            'name': 'Nome',
            'city': 'Città',
            'address': 'Indirizzo',
            'email': 'Email',
            'phone_number': 'Telefono',
            'p_iva': 'Partita IVA'
        }


class EditionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditionForm, self).__init__(*args, **kwargs)
        self.fields['publisher'].label = 'Editore'
        self.fields['book'].label = 'Libro'

    helper = FormHelper()
    helper.form_id = 'edition_entry_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Edition
        fields = '__all__'
        labels = {
            'isbn_digital': 'Codice ISBN digitale',
            'isbn_physical': 'Codice ISBN cartaceo',
            'pub_date': 'Data di pubblicazione',
            'language': 'Lingua',
            'pic': 'Copertina',
            'summary': 'Sinossi'
        }


class CopyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'copy_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    num_ebooks = forms.IntegerField(label='Copie di tipo ebook')
    num_audiobooks = forms.IntegerField(label='Copie di tipo audiolibro')
    ebook_file = forms.FileField(label='File ebook', required=False)
    audiobook_file = forms.FileField(label='File audiobook', required=False)

    class Meta:
        model = Copy
        fields = ('num_ebooks', 'num_audiobooks', 'ebook_file', 'audiobook_file')


    @transaction.atomic
    def save(self, *args, **kwargs):
        copy = super().save(commit=False)
        for num in range(0, int(self.cleaned_data.get('num_ebooks'))):
            copy_instance = Copy(edition=copy.edition,
                                 library_id=copy.library_id,
                                 is_audiobook=False,
                                 file=self.cleaned_data.get('ebook_file'))
            copy_instance.save()
        for num in range(0, int(self.cleaned_data.get('num_audiobooks'))):
            copy_instance = Copy(edition_id=copy.edition_id,
                                 library_id=copy.library_id,
                                 is_audiobook=True,
                                 file=self.cleaned_data.get('audiobook_file'))
            copy_instance.save()

        return copy_instance


class CopyUpdateForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'copy_update_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Copy
        fields = ('file',)
        labels = {
            'file': 'File'
        }


class BookListForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'book_list_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = BookList
        fields = ('title', 'desc', 'is_private')
        labels = {
            'title': 'Titolo',
            'desc': 'Descrizione',
            'is_private': ('Lista privata?'),
        }


class AddBookToListForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(AddBookToListForm, self).__init__(*args, **kwargs)
        self.fields['list'].queryset = BookList.objects.filter(owner=user)
        self.fields['list'].label = 'Lista'

    helper = FormHelper()
    helper.form_id = 'add_book_to_list_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = EditionInList
        fields = ('list',)



class PeriodicalForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PeriodicalForm, self).__init__(*args, **kwargs)
        self.fields['publisher'].label = 'Editore'
        self.fields['category'].label = 'Categoria'

    helper = FormHelper()
    helper.form_id = 'periodical_issue_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Periodical
        fields = '__all__'
        labels = {
            'name': 'Nome',
            'language': 'Lingua',
            'is_newspaper': 'Giornale?',
            'pic': 'Copertina',
        }


class PeriodicalIssueForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'periodical_issue_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = Issue
        fields = ('number', 'pub_date', 'file')
        labels = {
            'number': 'Numero',
            'pub_date': 'Data di pubblicazione',
            'file': 'File',
        }



