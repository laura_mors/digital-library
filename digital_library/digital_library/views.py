from django.views.generic import TemplateView

from digital_lending.models import Edition, Periodical, BookList, Issue
from digital_lending.utils import check_available_reserved_books


class Homepage(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)
        latest_book_editions = Edition.objects.order_by('-pub_date')[:5]
        latest_periodical_issues = Issue.objects.order_by('-pub_date')[:5]
        latest_book_lists = BookList.objects.filter(is_private=False).order_by('-date_created')[:5]
        context['latest_book_editions'] = latest_book_editions
        context['latest_periodical_issues'] = latest_periodical_issues
        context['latest_book_lists'] = latest_book_lists
        if self.request.user.is_authenticated and self.request.user.is_person:
            context['available_reserved_books'] = check_available_reserved_books(self.request.user)
        return context


class NotFound(TemplateView):
    template_name = '404.html'
