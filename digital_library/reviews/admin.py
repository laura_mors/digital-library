from django.contrib import admin

from reviews.models import Review

@admin.register(Review)
class EditionAdmin(admin.ModelAdmin):
    list_display = ("author", "book", "rating", 'text', 'date_created', 'upvotes', 'downvotes')
    list_filter = ('rating', )
    search_fields = ("book__book__title__icontains", "author__username__icontains",)

    def upvotes(self, obj):
        upvotes = obj.upvotes.all().count()
        return upvotes

    def downvotes(self, obj):
        downvotes = obj.downvotes.all().count()
        return downvotes

