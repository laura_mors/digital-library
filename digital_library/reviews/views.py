from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from digital_lending.models import Edition
from .forms import ReviewCreateForm, ReviewUpdateForm
from .models import Review


class ReviewListView(ListView):
    model = Review
    template_name = 'reviews/list.html'
    paginate_by = 6

    def get_queryset(self):
        reviews = Review.objects.filter(book=self.kwargs['pk'])

        order = self.request.GET.get("sort")
        if order == "oldest":
            reviews = reviews.order_by("-date_created")
        elif order == "best_rating":
            reviews = reviews.order_by("-rating")
        elif order == "worst_rating":
            reviews = reviews.order_by("rating")
        else:
            reviews = reviews.order_by("date_created")

        return reviews

    def get_context_data(self, **kwargs):
        context = super(ReviewListView, self).get_context_data(**kwargs)
        context['edition'] = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        context['sort'] = self.request.GET.get("sort")
        return context


class PersonReviewList(ListView):
    paginate_by = 6
    model = Review
    template_name = 'reviews/personal_list.html'

    def get_queryset(self):
        reviews = Review.objects.filter(author_id=self.request.user.id).order_by('-date_created')
        return reviews


class ReviewCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    form_class = ReviewCreateForm
    template_name = "reviews/create.html"
    model = Review
    success_message = 'Recensione creata con successo!'

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('reviews:review-list', kwargs={'pk': pk})

    def get_context_data(self, **kwargs):
        context = super(ReviewCreateView, self).get_context_data(**kwargs)
        context['edition'] = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return context

    def get_form(self):
        form = super().get_form()
        form.instance.book = Edition.objects.get(isbn_digital=self.kwargs['pk'])
        return form

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        try:
            return super().post(request, *args, **kwargs)
        except IntegrityError:
            messages.add_message(request, messages.ERROR,
                                 'Hai già scritto una recensione per questa edizione.')
            return render(request, template_name=self.template_name, context=self.get_context_data())


class ReviewDetail(DetailView):
    model = Review
    template_name = 'reviews/detail.html'

    def get_context_data(self, **kwargs):
        context = super(ReviewDetail, self).get_context_data(**kwargs)
        context['upvotes'] = Review.objects.get(id=self.kwargs['pk']).upvotes.all().count()
        context['downvotes'] = Review.objects.get(id=self.kwargs['pk']).downvotes.all().count()
        return context


class ReviewUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Review
    template_name = 'reviews/update.html'
    form_class = ReviewUpdateForm
    success_message = 'Recensione aggiornata con successo!'

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('reviews:review-detail', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(ReviewUpdateView, self).get_object(*args, **kwargs)
        if not obj.author_id == self.request.user.id:
            raise PermissionDenied
        return obj


class ReviewDeleteView(LoginRequiredMixin, DeleteView):
    model = Review
    template_name = 'reviews/delete.html'
    success_message = 'Recensione eliminata con successo!'

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('reviews:my-reviews')

    def delete(self, request, *args, **kwargs):
        obj = super(ReviewDeleteView, self).delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return obj

    def get_object(self, *args, **kwargs):
        obj = super(ReviewDeleteView, self).get_object(*args, **kwargs)
        if not obj.author_id == self.request.user.id:
            raise PermissionDenied
        return obj


class ReviewUpvoteView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        review = Review.objects.get(id=self.kwargs['pk'])
        if review.upvotes.filter(id=request.user.id).exists():
            review.upvotes.remove(request.user)
        else:
            review.upvotes.add(request.user)
            if review.downvotes.filter(id=request.user.id).exists():
                review.downvotes.remove(request.user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class ReviewDownvoteView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        review = Review.objects.get(id=self.kwargs['pk'])
        if review.downvotes.filter(id=request.user.id).exists():
            review.downvotes.remove(request.user)
        else:
            review.downvotes.add(request.user)
            if review.upvotes.filter(id=request.user.id).exists():
                review.upvotes.remove(request.user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
