from django.urls import path
from . import views
from .views import *

app_name = 'reviews'

urlpatterns = [
    path('edition/<int:pk>/reviews', ReviewListView.as_view(), name="review-list"),
    path('my-reviews', PersonReviewList.as_view(), name='my-reviews'),
    path('<int:pk>/detail', ReviewDetail.as_view(), name="review-detail"),
    path('edition/<int:pk>/add-review', ReviewCreateView.as_view(), name="review-create"),
    path('<int:pk>/update-review', ReviewUpdateView.as_view(), name="review-update"),
    path('<int:pk>/delete-review', ReviewDeleteView.as_view(), name="review-delete"),
    path('<int:pk>/upvote', ReviewUpvoteView.as_view(), name="review-upvote"),
    path('<int:pk>/downvote', ReviewDownvoteView.as_view(), name="review-downvote"),
]

