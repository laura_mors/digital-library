from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from digital_lending.models import Edition
from accounts.models import User


class Review(models.Model):
    author = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    book = models.ForeignKey(Edition, blank=False, null=False, on_delete=models.CASCADE)
    rating = models.IntegerField(
        validators=[MaxValueValidator(10), MinValueValidator(1)],
        blank=False,
        null=False
    )

    text = models.TextField(max_length=9000)
    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    upvotes = models.ManyToManyField(User, symmetrical=False, related_name='upvotes', blank=True)
    downvotes = models.ManyToManyField(User, symmetrical=False, related_name='downvotes', blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["author", "book"],
                name="Each user can review a book only once",
            )
        ]

