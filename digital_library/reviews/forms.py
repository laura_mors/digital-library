from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from .models import Review


class ReviewCreateForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'create_review_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta(forms.ModelForm):
        model = Review
        fields = ("rating", "text")
        labels = {
            'rating': 'Voto',
            'text': 'Testo',
        }


class ReviewUpdateForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'update_review_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Submit'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta(forms.ModelForm):
        model = Review
        fields = ("rating", "text")
        labels = {
            'rating': 'Voto',
            'text': 'Testo',
        }
