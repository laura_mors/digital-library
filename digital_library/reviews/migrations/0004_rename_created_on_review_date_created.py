# Generated by Django 3.2.4 on 2021-08-24 09:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review',
            old_name='created_on',
            new_name='date_created',
        ),
    ]
