# Digital Library
Con questa applicazione web si desidera gestire e raccogliere le informazioni relative ad un portale bibliotecario digitale, cioè una rete di biblioteche italiane che mettono a disposizione le risorse dei loro archivi digitali per poter essere consultate dagli utenti iscritti a tali biblioteche.


### Descrizione

Gli utenti possono essere di due tipi: persone o biblioteche. 
Gli utenti di tipo biblioteca aggiungono le risorse digitali che possiedono al sistema per renderle disponibili ai loro iscritti. Questi ultimi sono gli utenti di tipo persona, che possono aggiungere le biblioteche a cui sono iscritti dal loro profilo, per poi consultare e prendere in prestito le risorse che le loro biblioteche forniscono. 
La totalità delle risorse offerte da ogni biblioteca è visualizzabile da ogni tipo di utente, inclusi quelli anonimi, che possono navigare il sito ma non possono interagire con le risorse.
Vi è inoltre l’utente amministratore.

Entrambi i tipi di  utenti possono creare il proprio account e gestirlo, visualizzare e consultare le risorse che la piattaforma offre, creare liste, e scrivere recensioni. 
Le biblioteche possono inoltre aggiungere risorse digitali e gestirle. Sono queste le risorse con cui le persone interagiranno, prendendole in prestito e/o prenotandole, o semplicemente consultandole (nel caso dei periodici). 
Le persone possono inoltre suggerire alle loro biblioteche di acquistare risorse che attualmente non hanno, aggiungere risorse ai preferiti, ed aggiungere e gestire le loro iscrizioni alle biblioteche tramite le tessere bibliotecarie che le biblioteche fisiche forniscono.

### Istruzioni per l'installazione
- Clonare la repository con il comando `git clone https://gitlab.com/laura_mors/digital-library.git`;
- Spostarsi nella cartella `digital_library/digital_library`;
- Eseguire il comando `pipenv install django`;
- Entrare nel virtual environment creato con il comando `pipenv shell`;
- Installare i requisiti eseguendo il comando `pip install -r requirements.txt`;
- Eseguire il comando `python manage.py runserver` e andare all’indirizzo http://127.0.0.1:8000.

È già presente un database con vari dati di prova.
L’utente admin ha come username “admin” e password “provadmin”. 

Username degli utenti di tipo persona: Alice, Bob, Alex, Eli.

Username degli utenti di tipo biblioteca: BiblioLoria, Delfini, Biblioteca_Centrale, BiblioCornelia.

Gli utenti hanno tutti come password “profiloprova”.

Il server non è idoneo alla distribuzione.

